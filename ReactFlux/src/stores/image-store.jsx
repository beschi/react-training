var Reflux = require('reflux');
var Api = require('../utils/api');
var Actions = require('../actions');
var _ = require('lodash');

module.exports = Reflux.createStore({
    listenables: [Actions],
    getImages: function(topicId) {
        return Api.get('topics/' + topicId)
            .then(function(json) {
                this.images = _.reject(json.data, function(image) {
                    return image.is_album;
                });
                // this.images = json.data;
                this.triggerChange();
            }.bind(this));
    },
    getImage: function(inputId) {
        Api.get('gallery/image/' + inputId)
            .then(function(json) {
                if (this.images) {
                    this.images.push(json.data);
                } else {
                    this.images = [json.data];
                }
                this.triggerChange();
            }.bind(this));
    },
    find: function(inputId) {
        var image = _.find(this.images, {id: inputId});

        if (image) {
            return image;
        } else {
            this.getImage(inputId);
            return null;
        }
    },
    triggerChange: function() {
        this.trigger('change', this.images);
    }
});
