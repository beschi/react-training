var Reflux = require('reflux');

//list of names - same as methodss in topic store
module.exports = Reflux.createActions([
    'getTopics',
    'getImages',
    'getImage'
]);
